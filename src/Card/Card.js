import React, {Component} from 'react';

const classes = {
    "D": "diams",
    "H": "hearts",
    "S": "spades",
    "C": "clubs",
};

const htmlEnt = {
    "H": "♥",
    "D": "♦",
    "C": "♣",
    "S": "♠",
};


class Card extends Component {
    render() {
        return (
            <div className={`card rank-${this.props.rank.toString().toLowerCase()} ${classes[this.props.suit]}`}>
                <span className="rank">{this.props.rank}</span>
                <span className="suit">{htmlEnt[this.props.suit]}</span>
            </div>
        );
    }
}

export default Card;