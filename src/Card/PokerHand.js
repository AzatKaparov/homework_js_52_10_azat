class PokerHand {
    constructor(massive) {
        this.cards = massive;
    };

    _findPair = () => {
        const filtered = [...new Set(this.cards.map(item => item.ranks))];
        if (filtered.length === 4) {
            return "Pair";
        } else {
            return false;
        }
    };

    _findTwoOrThree = () => {
        let counter = 0;
        for (let i = 0; i < this.cards.length; i++) {
            for (let j = i + 1; j < this.cards.length; j++) {
                if (this.cards[i].ranks === this.cards[j].ranks) {
                    counter++;
                }
            }
        }
        switch (counter) {
            case 2:
                return "Two pairs";
            case 3:
                return "Three of a kind";
            default:
                return false;
        }
    };

    _findFlash = () => {
        const filtered = [...new Set(this.cards.map(item => item.suit))];
        if (filtered.length === 1) {
            return "Flush";
        } else {
            return false;
        }
    };

    getOutcome() {
        const checks = [this._findPair(), this._findTwoOrThree(), this._findFlash()];
        let combination = null;
        checks.forEach(item => {
            if (item !== false) {
                combination = item;
            }
        });
        if (combination === null) {
            combination = "Highest card";
        }
        return combination;
    };
}

export default PokerHand;