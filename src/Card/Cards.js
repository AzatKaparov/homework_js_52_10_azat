import React, {Component} from 'react';
import Card from "./Card";
import CardDeck from "./CardDeck";
import PokerHand from "./PokerHand";


class Cards extends Component {
    constructor(props) {
        super(props);
        const startDeck = new CardDeck();
        this.state = {
            deck: startDeck,
            cards: startDeck.getCards(5),
            noMoreCards: false,
            money: 0,
        };
    }

    _addMoney = () => {
        const hand = new PokerHand(this.state.cards);
        const result = hand.getOutcome();
        switch (result) {
            case "Highest card":
                this.setState({money: this.state.money + 5});
                break;
            case "Pair":
                this.setState({money: this.state.money + 10});
                break;
            case "Two pairs":
                this.setState({money: this.state.money + 20});
                break;
            case "Three of a kind":
                this.setState({money: this.state.money + 50});
                break;
            case "Flush":
                this.setState({money: this.state.money + 100});
                break;
            default:
                break;
        }
    };

    getNewDeck = () => {
        const newDeck = new CardDeck();
        const newCards = newDeck.getCards(5);
        this.setState({
            cards: newCards,
            deck: newDeck,
            noMoreCards: false,
        });
        this._addMoney();
    };

    getAnotherCards = () => {
        const newDeck = this.state.deck;
        if (newDeck.cards.length === 2){
            this.setState({noMoreCards: true});
        } else {
            const newCards = newDeck.getCards(5);
            this.setState({
                cards: newCards,
                deck: newDeck});
            this._addMoney();
        }
    };

    showCombination = () => {
        const hand = new PokerHand(this.state.cards);
        return hand.getOutcome();
    };


    render() {
        return (
            <div className="playingCards content">
                <div className='deck-row'>
                    <h3>Your balance: {this.state.money} $</h3>
                    <h3>Cards in deck: {this.state.deck.cards.length}</h3>
                    {this.state.noMoreCards &&
                        <h2>You can't take more cards now!</h2>
                    }
                    <p>This button will give you 5 cards from
                        <span className="deck-type new-deck">NEW</span>
                        deck</p>
                    <button
                        className='deck-btn'
                        onClick={this.getNewDeck}
                    >
                        get new deck</button>
                    <p>This button will give you 5 cards from
                        <span className="deck-type old-deck">OLD</span>
                        deck</p>
                    <button
                        className='deck-btn'
                        disabled={this.state.noMoreCards}
                        onClick={this.getAnotherCards}
                    >
                        get another cards</button>
                </div>
                {this.state.cards.map((card, idx) => {
                    return (
                        <Card rank={card.ranks} suit={card.suit} key={idx}/>
                    )
                })}
                <h1>{this.showCombination()}</h1>
            </div>
        );
    }
}

export default Cards;