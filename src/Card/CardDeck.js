const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};


class CardDeck {
    constructor() {
        const suits = ["H", "D", "S", "C"];
        const variants = [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A"];
        const newDeck = [];
        suits.forEach(suit => {
            variants.forEach(variant => {
                const newCard = {
                    suit: suit,
                    ranks: variant
                };
                newDeck.push(newCard);
            });
        });
        this.cards = newDeck;
    };

    getCard() {
        const random = getRandomInt(0, this.cards.length - 1);
         return this.cards.splice(random, 1)[0];
    };

    getCards(howMany) {
        const cardMassive = [];
        while (cardMassive.length !== howMany) {
            cardMassive.push(this.getCard());
        }
        return cardMassive;
    };
}

export default CardDeck;